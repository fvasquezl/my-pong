# Create and move a paddle (with=20, height=100,x_pos=350 y_pos=0)
# ShapeSize(stretch_wid=5,stretch_len=1) each square=20 ... 20x5=100, 20,1=20

from turtle import Turtle


class Paddle(Turtle):
    def __init__(self, position):
        super().__init__()
        self.shape('square')
        self.color('white')
        self.shapesize(stretch_wid=5,stretch_len=1)
        self.penup()
        self.goto(position)
        self.points = 0

    def go_up(self):
        if self.ycor() < 240:
            new_y = self.ycor() + 20
            self.goto(self.xcor(), new_y)

    def go_down(self):
        if self.ycor() > -240:
            new_y = self.ycor() - 20
            self.goto(self.xcor(), new_y)

    def set_point(self):
        self.points += 1


